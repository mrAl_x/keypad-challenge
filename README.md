# Keypad Challenge
## Description
This is a simple emulation of a virtual keypad.

#### Notes
Access pin is **1223**.
The user will be locked after *3 failed attempts*.

## Running the project

### Installation
In order to run this app you'll need to install its dependencies via **NPM**:

`$ npm install`

### Build
To build the project you can run:

`$ npm run build`

The bundled project should be under the `/build` folder in the root of the project.

### Development Server
To startup a local server you can run:

`$ npm start`

## A11Y
This app is fully accessible so feel free to explore it with the keyboard.

Plus if you're running **MacOS**, try pressing *CMD + F5* to enable the screen reader and experience the audible goodness :)
