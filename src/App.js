import React, { Component } from 'react';
import './App.css';
import classnames from 'classnames';
import Data from './data/Data';

// Components
import { Keypad, Visor } from './components';

import styles from './App.css';

class App extends Component {
	state = {
		hasError: false,
		isSuccessful: false,
		isUserLocked: false,
		strikes: 0,
		userInput: [],
	};

	componentWillMount() {
		if (localStorage.getItem("isUserLocked")) {
			this.setState({ isUserLocked: true });
			this.unlockUser();
		}
	}

	componentDidUpdate(prevProps, prevState) {
		const {
			userInput,
			hasError,
			isSuccessful,
			strikes,
			isUserLocked,
		} = this.state;

		if (hasError) {
			/* Add a strike and return the App's original state after a timeout */
			const TIMER_MILLISECONDS = 800;
			this.resetStateMessage(
				{
					hasError: false,
					userInput: [],
					strikes: (prevState.strikes += 1),
				},
				TIMER_MILLISECONDS
			);
		}

		if (isSuccessful) {
			/* Return the App's original state after a timeout */
			const TIMER_MILLISECONDS = 3000;
			this.resetStateMessage(
				{
					isSuccessful: false,
					userInput: [],
					strikes: 0,
				},
				TIMER_MILLISECONDS
			);
		}

		if (strikes === 3 && !isUserLocked) {
			this.lockUser();
		}

		if (isUserLocked) {
			this.unlockUserDefaultTimer();
		}

		if (userInput.length === 4 && !hasError && !isSuccessful) {
			this.submitPin(userInput);
		}
	}

	render() {
		const { userInput, isSuccessful, hasError, isUserLocked } = this.state;
		const wrapperClasses = classnames(
			styles.wrapper,
			{ [styles.wrapper__error]: hasError },
			{ [styles.wrapper__success]: isSuccessful }
		);

		return (
			<div className={styles.app}>
				<div className={wrapperClasses}>
					<Visor
						userInput={userInput}
						isSuccessful={isSuccessful}
						isUserLocked={isUserLocked}
						hasError={hasError}
					/>
					<Keypad
						isUserLocked={isUserLocked}
						submitDigit={digit => this.submitDigit(digit)}
					/>
				</div>
			</div>
		);
	}

	/* submitDigit is triggered on each <KeyPad /> onClick */
	submitDigit = digit => {
		const { userInput } = this.state;

		if (userInput.length < 4) {
			/* Add the new input digit to the current pin */
			this.setState(prevState => {
				const userInput = prevState.userInput.concat(digit);

				return { userInput };
			});
		}
	};

	/* Convert the userInput array to a number and store it on the state */
	submitPin = pinArray => {
		const pin = Number(pinArray.join(""));

		if (pin === Data.defaultPin) {
			return this.setState({ isSuccessful: true });
		}

		return this.setState({ hasError: true });
	};

	/* Clear either the error message or the successfull message after some time */
	resetStateMessage = (nextState, timer) =>
		setTimeout(() => this.setState(nextState), timer);

	/* Prevent the user to input anything for a predetermined amount of time.
	I'm saving the timestamp in the localStorage so the user can't simply refresh the page
	to ignore the "lockdown" */
	lockUser = () => {
		const timeStamp = Math.floor(Date.now() / 1000);

		this.setState({
			isUserLocked: true,
		});

		localStorage.setItem("isUserLocked", true);
		localStorage.setItem("lockdownTimestamp", timeStamp);
	};

	/* Returns the number of seconds that have passed since the lockdown */
	secondsAfterLockdown = () => {
		const prevTimestamp = localStorage.getItem("lockdownTimestamp");
		const currentTimestamp = Math.floor(Date.now() / 1000);

		return currentTimestamp - prevTimestamp;
	};

	/* Checks the number of seconds since the lockdown and unlocks the app after that time */
	unlockUser = () => {
		const TIMEOUT_IN_SECONDS = 10;
		const TIMER_MILLISECONDS = this.secondsAfterLockdown() * 1000;

		if (this.secondsAfterLockdown() >= TIMEOUT_IN_SECONDS) {
			setTimeout(this.resetLockdown(), TIMER_MILLISECONDS);
		}
	};

	/* Unlocks the app after 10 seconds */
	unlockUserDefaultTimer = () => {
		const TIMEOUT_IN_SECONDS = 10;
		setTimeout(() => this.resetLockdown(), TIMEOUT_IN_SECONDS * 1000);
	};

	/* Clears all data that sets the app on lockdown */
	resetLockdown = () => {
		localStorage.clear();
		this.setState({ isUserLocked: false, strikes: 0 });
	};
}

export default App;
