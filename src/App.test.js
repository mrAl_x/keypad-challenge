import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

class LocalStorageMock {
	constructor() {
		this.store = {};
	}

	clear() {
		this.store = {};
	}

	getItem(key) {
		return this.store[key] || null;
	}

	setItem(key, value) {
		this.store[key] = value.toString();
	}

	removeItem(key) {
		delete this.store[key];
	}
};

global.localStorage = new LocalStorageMock;

describe('<App />', () => {
	it('should render correctly', () => {
		const tree = shallow(
			<App />
		);

		expect(tree).toMatchSnapshot();
	});

	describe('On user input', () => {
		const tree = shallow(
			<App />
		);

		it('should add a digit', () =>{
			tree.instance().submitDigit(1);
			tree.instance().submitDigit(1);

			expect(tree.state('userInput')).toEqual([1, 1]);
			expect(tree.state('userInput')).toHaveLength(2);
		});

		it('should submit a wrong pin code', () => {
			tree.setState({
				isSuccessful: false,
				hasError: false,
				userInput: [1, 2, 3, 4],
			});

			expect(tree.state('hasError')).toBe(true);
			expect(tree.state('isSuccessful')).toBe(false);
		});

		it("should submit the correct pin code", () => {
			tree.setState({
				isSuccessful: false,
				hasError: false,
				userInput: [1, 2, 2, 3],
			});

			expect(tree.state("hasError")).toBe(false);
			expect(tree.state("isSuccessful")).toBe(true);
		});
	});

	describe('On successful message', () => {
		it('should update the state', () => {
			const tree = shallow(
				<App />
			);
			const spy = jest.spyOn(tree.instance(), 'resetStateMessage');

			tree.setState({
				isSuccessful: true,
			});

			expect(spy).toHaveBeenCalledTimes(1);
		});
	});

	describe('On three failed attempts', () => {
		it('should lock the user', () => {
			const tree = shallow(<App />);
			tree.setState({ strikes: 3, isUserLocked: false });

			expect(tree.state('isUserLocked')).toBe(true);
		});
	});

	describe('When the user is locked', () => {
		jest.useFakeTimers();
		it('should be unlocked after 10 seconds', () => {
			const tree = shallow(<App />);
			const spy = jest.spyOn(tree.instance(), "unlockUserDefaultTimer");
			tree.setState({ isUserLocked: true });
			jest.runAllTimers();

			expect(spy).toHaveBeenCalledTimes(1);
			expect(tree.state('isUserLocked')).toBe(false);
			expect(tree.state('strikes')).toBe(0);
		});
	});
});