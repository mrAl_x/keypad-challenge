import React from 'react';
import { shallow } from 'enzyme';
import Keypad from './Keypad';

describe('<Keypad />', () => {
	it('should render correctly', () => {
		const tree = shallow(
			<Keypad submitDigit={ () => true } />
		);

		expect(tree).toMatchSnapshot();
	});

	it('should click a button', () => {
		const onClick = jest.fn();
		const tree = shallow(
			<Keypad submitDigit={ onClick } />
		);

		const buttonTree = tree.find('button').at(0);
		buttonTree.simulate('click');

		expect(onClick).toHaveBeenCalledTimes(1);
	});
});