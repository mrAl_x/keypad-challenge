import React from 'react';
import PropTypes from "prop-types";
import classnames from 'classnames';

// Styles
import styles from './Keypad.css';

const Keypad = ({ submitDigit, isUserLocked }) => {
	const keypad = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

	const handleOnClick = (digit) => {
		submitDigit(digit);
	}

	const buttonClasses = (index) => classnames(
		styles.button,
		{ [styles.button__lineBreak]: index === 0 }
	);

	/*
		Give all the buttons but the '0' a tabIndex="1" so the :focus behaviour
		respects the visual order when the user presses Tab with the keyboard
	*/
	const defineTabIndex = (index) => index === 0 ? 0 : 1;

	return (
		<div className={ styles.keypad }>
			{
				keypad.map((digit, index) => (
					<button
						key={ index }
						tabIndex={ defineTabIndex(index) }
						className={ buttonClasses(index) }
						disabled={ isUserLocked }
						onClick={ () => handleOnClick(digit) }>
						{ digit }
					</button>
				))
			}
		</div>
	);
};

Keypad.defaultProps = {
	isUserLocked: false,
};

Keypad.propTypes = {
	submitDigit: PropTypes.func.isRequired,
	isUserLocked: PropTypes.bool.isRequired,
};

export default Keypad;
