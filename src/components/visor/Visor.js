import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

// Styles
import styles from './Visor.css';

class Visor extends Component {
	render() {
		const { userInput, hasError, isSuccessful, isUserLocked } = this.props;

		return (
			<div className={styles.visor} aria-live="assertive">
				{ hasError && this.renderErrorMessage() }
				{ isSuccessful && this.renderSuccessMessage() }
				{ isUserLocked && this.renderLockedMessage() }
				{ (!hasError && !isSuccessful ) && this.hideDigits(userInput) }
			</div>
		);
	}

	/* Hide each digit but the last input */
	hideDigits = digitsArray =>
		digitsArray.map((digit, index) => {
			if (index === digitsArray.length - 1) {
				return digit;
			}

			return "•";
		});

	renderErrorMessage = () => (
		<span className={ classnames(styles.message, styles.message__error) }>
			Wrong pin
		</span>
	);

	renderSuccessMessage = () => (
		<span className={ classnames(styles.message, styles.message__success) }>
			Welcome! <span role="img" aria-label="clown emoji">🤡</span>
		</span>
	);

	renderLockedMessage = () => (
		<span className={ classnames(styles.message, styles.message__lockDown) }>
			You've been locked out. You can try again in 10 seconds
		</span>
	);
}

Visor.propTypes = {
	userInput: PropTypes.array.isRequired,
	hasError: PropTypes.bool,
	isSuccessful: PropTypes.bool,
	isUserLocked: PropTypes.bool,
};

export default Visor;
