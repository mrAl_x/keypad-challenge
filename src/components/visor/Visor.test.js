import React from 'react';
import { shallow } from 'enzyme';
import Visor from './Visor';

describe('<Visor />', () => {
	const userInput = [];

	it('should render correctly', () => {
		const tree = shallow(
			<Visor userInput={userInput} />
		);

		expect(tree).toMatchSnapshot();
	});

	it('should display an error message', () => {
		const tree = shallow(
			<Visor userInput={userInput} hasError />
		);

		expect(tree).toMatchSnapshot();
	});

	it('should display a success message', () => {
		const tree = shallow(
			<Visor userInput={userInput} isSuccessful />
		);

		expect(tree).toMatchSnapshot();
	});

	it('should display a "user is locked" message', () => {
		const tree = shallow(
			<Visor userInput={userInput} isUserLocked />
		);

		expect(tree).toMatchSnapshot();
	});
});
